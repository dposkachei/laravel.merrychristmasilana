<?php

use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app(Router::class);

$api->version('v1', function ($api) {
    $api->group([
        'middleware' => ['api'],
    ], function ($api) {
        $api->any('/', ['as' => 'index', 'uses' => \App\Http\Controllers\ApiController::class . '@index']);
        $api->any('/data', ['as' => 'index', 'uses' => \App\Http\Controllers\ApiController::class . '@data']);
        $api->any('/question', ['as' => 'index', 'uses' => \App\Http\Controllers\ApiController::class . '@question']);
        $api->any('/answer', ['as' => 'index', 'uses' => \App\Http\Controllers\ApiController::class . '@answer']);
        $api->any('/hint', ['as' => 'index', 'uses' => \App\Http\Controllers\ApiController::class . '@hint']);
    });
});
