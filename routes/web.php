<?php

use \Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'domain' => config('app.url'),
], function () {
    Route::group([
        'middleware' => ['guest'],
    ], function () {
        Route::get('/', ['uses' => '\\' . IndexController::class . '@index', 'as' => 'index']);
    });

    Route::group([
        'prefix' => 'lk',
        'middleware' => ['auth'],
    ], function () {
        Route::get('/', ['uses' => '\\' . IndexController::class . '@lk', 'as' => 'lk']);
    });

    Route::group([
        'prefix' => 'action',
        'as' => 'action.',
    ], function () {
        Route::post('/confirm/rules', ['uses' => '\\' . IndexController::class . '@actionConfirmRules', 'as' => 'confirm.rules']);
    });



    Route::post('/auth/login', ['uses' => '\\' . LoginController::class . '@login', 'as' => 'login.post']);
    Route::get('/auth/logout', ['uses' => '\\' . LoginController::class . '@logout', 'as' => 'logout.post']);
});
