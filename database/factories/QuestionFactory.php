<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Question::class, function (Faker $faker) {
    return [
        'number' => $faker->unique()->randomDigit,
        'name' => $faker->slug,
        'title' => $faker->name,
        'description' => $faker->realText(),
        'purpose' => $faker->sentence,
        'answer' => $faker->word,
    ];
})->afterCreating(\App\Models\Question::class, function ($oItem) {
//    factory(\App\Models\QuestionHint::class)->create([
//        'question_id' => $oItem->id,
//    ]);
});
