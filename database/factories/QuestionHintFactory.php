<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\QuestionHint::class, function (Faker $faker, $data) {
    if (!isset($data['question_id'])) {
        $oQuestion = factory(\App\Models\Question::class)->create();
        $data['question_id'] = $oQuestion->id;
    }
    return [
        'question_id' => $data['question_id'],
        'text' => $faker->sentence,
        'priority' => rand(0, 100),
        'status' => 1,
    ];
});
