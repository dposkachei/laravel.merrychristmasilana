<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->questionLite(1);
        $this->questionJuice(2);
        $this->questionFlower(3);
        $this->questionBorder(4);
        $this->questionNotes(5);
        $this->questionBrother(6);
        $this->questionSong(7);
        $this->questionSearch(8);
        $this->questionLove(9);
    }


    public function questionLite($number)
    {
        $text = '
            Доброе утро <b>Дурында</b>, пока я все еще сплю, предлагаю тебе поиграть в <b>квест</b>. <br>
            Тебя ждут конкурсыЫЫыы, загадки и т.п. <br>
            Новый год же скоро, юхуу!!!!!!! <br>
            Чтобы сильно не томить, вот первая загадка: <br>
            <blockquote>"Кто ты?"</blockquote>
        ';
        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'lite-question',
            'title' => 'Разминка',
            'description' => $text,
            'purpose' => '* это не имя',
            'answer' => 'дурында',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Ну на Д начинается...',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Не будет подсказки!',
        ]);
        return $oQuestion;
    }

    public function questionJuice($number)
    {
        $text = '
        Вот ты и признала это, все ради этого, расходимся ХД <br>
        Лады, доиграем уже 8D <br>
        Давай простенький вопрос на сообразительность:
        <blockquote>
        "Как правильно сок или сёк?"
        </blockquote>
        ';
        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'juice-question',
            'title' => 'Не самый сложный',
            'description' => $text,
            'purpose' => null,
            'answer' => 'сёк',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Ну это же, с*к',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'А если так уже? *ё*, попробуй не отгадать...((',
            'priority' => 5,
        ]);
        return $oQuestion;
    }

    public function questionFlower($number)
    {
        $text = '
        А теперь поиграем по взрослому:
        <blockquote>"Он мне сразу не понравился, не может запомнить моё название и говорит, что я вру, да еще и бьет периодически. Кто я?"</blockquote>
        ';
        // Находит цветок
        // Там загадка с властелина колец и она понимает что это ВРЕМЯ

        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'flower-question',
            'title' => 'Он плохой',
            'description' => $text,
            'purpose' => '* введи ответ не на это',
            'answer' => 'время',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Эта та еще мразота',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Да не буду я тебе показывать пальцем, но это тот еще Джеки Чан',
            'priority' => 8,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Это цветок, китайсякий...',
            'priority' => 7,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Внутри короче ищи',
            'priority' => 4,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Капайся, мухахахахаха',
            'priority' => 3,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Ну а то что там нашла, гугл в помощь тогда',
            'priority' => 2,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Time по русски епта',
            'priority' => 1,
        ]);
        return $oQuestion;
    }

    public function questionBorder($number)
    {
        $text = '
        Харош, поздравляю, ты теперь вся в грязи, надеюсь <br>
        Хи хи хи х и <br>
        Как теперь справишься с этой:
        <blockquote>
        "Узнаешь дальше путь от друга, это может быть и Кори, и Паук, а может это просто Жук, главное что его уже почти 1.5(не считая двух) и он постоянно тебе что-то показывает. <br>
        Не шелохнувшись тебе расскажет, что да как, но и не забудь поставить меня на место. Кто я?"
        </blockquote>
        ';
        // В рамке будет еще одна загадка
        // Надо написать Юле спеть песню Коржа
        // Юля напишет слово "гамункул"

        // На записке будет
        // "Спой Юле начало песни 'Фараденза', достаточно будет до припева"
        // Если ей понравится, то она скажет тебе ключевое слово

        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'border-question',
            'title' => 'Не мясо',
            'description' => $text,
            'purpose' => null,
            'answer' => 'гомункул',
        ]);
        $oQuestion->hints()->create([
            'text' => '.. не мясо',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Баба-',
            'priority' => 8,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Найди в комнате',
            'priority' => 5,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Это рамка вообще-то',
            'priority' => 4,
        ]);
        return $oQuestion;
    }



    public function questionNotes($number)
    {
        $text = '
        Поёшь ты превосходно, а я тоже творческий пацан, когда слюни не пускаю, вот стишок тебе посвятил:
        <blockquote>
        Как же сладок твой запах, <br>
        А ночью растает в нем сон <br>
        Как быть если нет в моих планах <br>
        Арбузов парочки тон <br>
        Шквал любви я получил <br>
        Кимано себе не купил <br>
        А с рифмой не дружу и не жужу
        </blockquote>
        ';
        // тут сопоставляет первые вуквы

        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'notes-question',
            'title' => 'Творческий конкурс',
            'description' => $text,
            'purpose' => 'Взято из стиха "Жу-жу"',
            'answer' => 'какашка',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Присмотрись к началу',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'К началу каждой',
            'priority' => 5,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Ну первые буквы, емаё',
            'priority' => 2,
        ]);
        return $oQuestion;
    }

    public function questionBrother($number)
    {
        $text = '
        <b>Какашка</b> ахахаха, по моему смешно. Ок идем дальше:
        <blockquote>
        В 8ой минуте десятка получишь ты ответ <br>
        Не спеши, и не медли, пока сделай омлет <br>
        От тебя необходимо только слово "Хой" <br>
        И из Брюховецкой он задаст тебе вопрос не простой.
        </blockquote>
        ';
        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'brother-question',
            'title' => 'Не раньше не позже',
            'description' => $text,
            'purpose' => '* на анлийском',
            'answer' => 'telegram',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Я хз спит он сейчас или нет',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Дикий скунс ...',
            'priority' => 8,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Звони, пиши, буди его, у него следующая загадка',
            'priority' => 7,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Whats up??',
            'priority' => 6,
        ]);
        return $oQuestion;
    }

    public function questionSong($number)
    {
        $text = '
        Чтож, теперь сложный вопрос, оооочень сложный, <br>
        Ты если что подсказки нажимай, а то я не знаю, справишься ли ты...
        <blockquote>
        "Я закрываю этот альбом и больше он меня не споет. Нет, я не тупая, я была 10я. Кто я?"
        </blockquote>
        <small>Когда найдешь меня, то обрати внимание на 14 и продолжи свой путь.</small>
        ';
        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'song-question',
            'title' => 'Отдохни',
            'description' => $text,
            'purpose' => '* найди слово или букву',
            'answer' => 'я',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Это песня твоей любимой группы',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'ЛП короче, ну але, я бы догадался',
            'priority' => 9,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Теперь найди эту песню. она последняя',
            'priority' => 8,
        ]);
        $oQuestion->hints()->create([
            'text' => '14я секунда этой песни, надеюсь ты нашла её I|',
            'priority' => 7,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Переведи на русский и найди это ... Ух ... чтож это',
            'priority' => 6,
        ]);
        $oQuestion->hints()->create([
            'text' => '... резать ими можно',
            'priority' => 5,
        ]);
        $oQuestion->hints()->create([
            'text' => 'А на них буква должна быть',
            'priority' => 4,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Это последняя буква алфавита ... Мдеее..',
            'priority' => 3,
        ]);
        return $oQuestion;
    }

    public function questionSearch($number)
    {
        $text = '
        Тут конечно ты долго справлялась, но все равно круть, вот это ты <b>Бось</b>.
        Следующая загадка:
        <blockquote>
        "Гуляют у речки рога да колечки"
        </blockquote>
        ';
        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'search-question',
            'title' => 'Ну ка найди',
            'description' => $text,
            'purpose' => null,
            'answer' => 'тебя',
        ]);
        $oQuestion->hints()->create([
            'text' => 'Найди этот предмет',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Там будет слово',
            'priority' => 8,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Можешь и так догадаться, следующее слово после Я ? ?',
            'priority' => 5,
        ]);
        return $oQuestion;
    }

    public function questionLove($number)
    {
        $text = '
        <blockquote>
        А теперь закончи предложение ...
        </blockquote>
        ';
        $oQuestion = factory(\App\Models\Question::class)->create([
            'number' => $number,
            'name' => 'love-question',
            'title' => 'Финалка',
            'description' => $text,
            'purpose' => null,
            'answer' => 'люблю',
        ]);
        $oQuestion->hints()->create([
            'text' => 'А тут не будет подсказки, сама думай',
            'priority' => 10,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Не не не',
            'priority' => 8,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Можешь не нажимать',
            'priority' => 6,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Не прокатит детка',
            'priority' => 4,
        ]);
        $oQuestion->hints()->create([
            'text' => 'Я тебя ..., ну ты и Балда Илановна',
            'priority' => 3,
        ]);
        return $oQuestion;
    }
}
