<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name' => 'Дурында',
            'email_verified_at' => now(),
            'email' => 'ilana89515131221@gmail.com',
            'password' => Hash::make('1234567890'),
            'remember_token' => \Illuminate\Support\Str::random(10),
        ]);
    }
}
