/*
 * @copyright Copyright (c) 2018-2019 "ИД Панорама"
 * @author Дмитрий Поскачей (dposkachei@gmail.com)
 */

let APP = {
    SOCKET: {
        ENABLED: true,
    },
};
export default { APP };
