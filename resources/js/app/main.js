$(document).ready(function () {
    $('.--page-html-content img').each(function () {
        let $img = $(this);
        $img.attr('data-fancybox', 'main');
        $img.attr('href', $img.attr('src'));
    });

    let window_width = $(window).width(),
        window_height = window.innerHeight,
        header_height = $('.default-header').height(),
        header_height_static = $('.site-header.static').outerHeight(),
        fitscreen = window_height - header_height;


    $('.fullscreen').css('height', window_height);
    $('.fitscreen').css('height', fitscreen);


    // -------   Active Mobile Menu-----//

    $('.menu-bar').on('click', function (e) {
        e.preventDefault();
        $('nav').toggleClass('hide');
        $('span', this).toggleClass('lnr-menu lnr-cross');
        $('.main-menu').addClass('mobile-menu');
    });
    $(document).ready(function () {
        $('#mc_embed_signup').find('form').ajaxChimp();
    });

    $(document).ready(function () {
        // $('.play-btn').magnificPopup({
        //     disableOn: 700,
        //     type: 'iframe',
        //     mainClass: 'mfp-fade',
        //     removalDelay: 160,
        //     preloader: false,
        //     fixedContentPos: false
        // });
    });
});
