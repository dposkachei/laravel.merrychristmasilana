/*
 * @copyright Copyright (c) 2018-2019 "ИД Панорама"
 * @author Дмитрий Поскачей (dposkachei@gmail.com)
 */
import Echo from 'laravel-echo';

window.io = require('socket.io-client');
/**
 * APP_URL window.Laravel.env.APP_URL
 *
 * @type {string}
 */
const host = window.Laravel.app.url;
console.log('------------- ECHO -------------');
console.log(host + ':6001');

window.Echo = new Echo({
    broadcaster: 'socket.io',
    host: host + ':6001',
    reconnectionAttempts: 5,
});
window.Echo.connector.socket.on('connect', function () {
    console.log('connected', window.Echo.socketId());
});
window.Echo.connector.socket.on('disconnect', function () {
    console.log('disconnected');
});
window.Echo.connector.socket.on('reconnecting', function (attemptNumber) {
    console.log('reconnecting', attemptNumber);
});
require('./listeners.js');





// window.Echo.private('user.messages')
//     .listen('NewMessageNotification', (e) => {
//         alert(e.message.message);
//     });

// let channelId = 1;
// window.Echo.private(`chat.${channelId}`)
//     .listen('ChatMessage', (e) => {
//         console.log('hi');
//     });

//let userId = $('meta[name="user_id"]').attr('content');
//console.log(userId);
// window.Echo.private(`user.${userId}`)
//     .listen('UserMessage', (e) => {
//         console.log('hi');
//     });

// window.Echo.join(`user.${userId}`)
//     .here((users) => {
//         console.log(users);
//     })
//     .joining((user) => {
//         console.log(user);
//     })
//     .leaving((user) => {
//         console.log(user);
//     });

// let checkOnlineCount = setInterval(function() {
//     axios.post(host + '/online').then(response => {
//         console.log(response.data.users);
//         notesStore.commit('ONLINE_USERS', response.data.users);
//     }).catch(error => {
//         console.log(error);
//     });
// }, 5000);

// window.socket.on('connection', (socket) => {
//     console.log('connection');
//
//
//     socket.on('disconnect', () => {
//         console.log('disconnect');
//     });
//     socket.on('Created', (data) => {
//         console.log('Created');
//         //socket.broadcast.emit('Created', (data));
//     });
// });
