/**
 * -------------------------------------------
 * Append laravel token
 * -------------------------------------------
 *
 */
try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {
}

import ENV from './config.js';

require('./helpers.js');

window.Laravel.app.csrf = $('meta[name="csrf-token"]').attr('content');
console.log(window.Laravel);

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': window.Laravel.app.csrf}
});

// let echo_enabled = ENV.APP.SOCKET.ENABLED && parseInt($('meta[name="echo"]').attr('content')) === 1 && window.Laravel.echo.enabled;
// if (echo_enabled) {
//     require('./broadcast/socket.js');
//     if (window.Laravel.echo.listen && !window.Laravel.reader.active) {
//         console.log(window.HELPER_ECHO.channel());
//         window.Echo.join(`${window.HELPER_ECHO.channel()}`)
//             .here((users) => {
//                 //axios.post(`/reader/user/${self.user.id}/online`, {});
//             });
//     }
//     if (!window.Laravel.user.guest && !window.Laravel.reader.active) {
//         console.log(window.HELPER_ECHO.userChannel());
//         window.Echo.private(window.HELPER_ECHO.userChannel());
//     }
// }



window.toastr = require('toastr/build/toastr.min.js');
require('./plugins/notification.js');
if (window.toastrOptions !== undefined) {
    window.toastr.options = window.toastrOptions;
}
if (window.toastrNotification !== undefined) {
    window.notification.send(window.toastrNotification);
}

require('./reader.js');


require('./callbacks.js');
require('./plugins/form.js');
//require('./plugins/cleave-masks.js');


require('./package/jquery.ajaxchimp.min.js');
require('./main.js');

require('../common/fancybox/fancybox.js');
require('../common/masks/cleave-masks.js');


require('../common/notification/notification.js');
