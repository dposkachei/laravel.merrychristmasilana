/**
 * Сам Vue
 *
 * https://ru.vuejs.org/v2/guide/
 */
window.Vue = require('vue');

window.axios = require('axios');

Vue.prototype.$http = window.axios;

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/**
 * Импортируем компоненты
 */
import _ from 'lodash';
import WaitComponent from './components/WaitComponent.vue';
import IndexComponent from './components/IndexComponent.vue';
import mixins from './mixins.js';


Vue.filter('two_digits', function (value) {
    if (value.toString().length <= 1) {
        return "0" + value.toString();
    }
    return value.toString();
});


Object.defineProperty(Vue.prototype, '$_', {value: _});

/**
 * Регистрируем его на id="app", если такой есть
 */
if (document.getElementById('wait')) {
    new Vue({
        el: '#wait',
        mixins: mixins,
        render: h => h(WaitComponent),
        data: {
            options: {
                modal: {
                    active: null,
                    tmpActive: null,
                },
            }
        },
    });
}
if (document.getElementById('app')) {
    new Vue({
        el: '#app',
        mixins: mixins,
        render: h => h(IndexComponent),
        data: {
            options: {
                modal: {
                    active: null,
                    tmpActive: null,
                },
            }
        },
    });
}
