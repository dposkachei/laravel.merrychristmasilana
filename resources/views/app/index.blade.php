@extends('app.layouts.app')

@section('content')
{{--    @include('app.layouts.components.banner')--}}
{{--    @include('app.layouts.components.video')--}}

    <section class="contact-area pt-100 pb-100 relative" style="height: 100vh;">
        <div class="overlay overlay-bg"></div>
        <div class="container --message-form-container">
            <div class="row">
                <div class="col-12">
                    <div class="story-content mb-100"
                        {{--                     style="padding: 10px; background-color: #ccc"--}}
                    >
                        <h1 class="text-white text-center" style="font-size: 20px;">
                            Добро пожаловать на квест под названием
                        </h1>
                        <h1 class="text-center text-white" style="line-height: 45px; font-size: 40px">
                            С Новым Годом
                            <p></p>
                            <span >
                                <span class="color-3">Д</span>
                                <span class="color-3">У</span>
                                <span class="color-3">Р</span>
                                <span class="color-3">Ы</span>
                                <span class="color-3">Н</span>
                                <span class="color-3">Д</span>
                                <span class="color-3">А</span>
                            </span>

                        </h1>
                        <p class="text-center">
                            Надеемся, что у вас сейчас такие же эмоции, как у этой замечательно девушке снизу.
                        </p>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <img class="d-flex mx-auto img-fluid" src="{{ asset('img/ezgif-4-4431e73ed0d2.gif') }}" alt="">
                </div>
                <div class="col-12 col-md-6 d-flex justify-content-center align-items-center jet-container {{ session()->get('confirm-rules') ? '' : 'j4et-active' }}">
                    <div class="jet-message">Сначала прочитай правила</div>
                    <div class="jet-opacity-lock" style="width: 300px">
                        <div class="row justify-content-center text-center">
                            <div class="single-contact col-12">
                                <h2 class="text-white">
                                    Войти
                                </h2>
                                <p class="text-white">
                                    Введите логин и пароль <br>
                                </p>
                            </div>
                        </div>
                        <form action="{{ route('login.post') }}" method="post" class="contact-form ajax-form">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <input name="email" placeholder="Email" class="common-input mt-20" required="" type="email">
                                </div>
                                <div class="col-12">
                                    <input name="password" placeholder="Пароль" class="common-input mt-20" required="" type="password">
                                </div>
                                <div class="col-12 d-flex justify-content-end">
                                    <button class="primary-btn d-inline-flex align-items-center mt-20 inner-form-submit" type="submit">
                                        <span>Войти</span>
                                    </button>
                                    <br>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
{{--    <section class="about-area pt-50">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="story-content">--}}
{{--                        <h2>--}}
{{--                            Правила--}}
{{--                        </h2>--}}
{{--                        <p class="mb-10 mt-10">--}}
{{--                            Чего делать нельзя:--}}
{{--                        </p>--}}
{{--                        <p class="mt-10">--}}
{{--                            1. Нельзя соблазнять создателя в целях получения подсказки.--}}
{{--                            <br>--}}
{{--                            2. Нельзя перескакивать на следующий уровень.--}}
{{--                            <br>--}}
{{--                            3. Нельзя взламывать сайт и переводить себя на следующий уровень.--}}
{{--                            <br>--}}
{{--                        </p>--}}
{{--                        <p class="mb-10 mt-10">--}}
{{--                            Что можно:--}}
{{--                        </p>--}}
{{--                        <p class="mt-10">--}}
{{--                            1. Нельзя соблазнять создателя в целях получения подсказки.--}}
{{--                            <br>--}}
{{--                            2. Нельзя перескакивать на следующий уровень.--}}
{{--                            <br>--}}
{{--                            3. Нельзя взламывать сайт и переводить себя на следующий уровень.--}}
{{--                            <br>--}}
{{--                        </p>--}}
{{--                        @if(!session()->get('confirm-rules'))--}}
{{--                            <a href="#" class="ajax-link primary-btn default-btn"--}}
{{--                               action="{{ route('action.confirm.rules') }}"--}}
{{--                               data-loading="1"--}}
{{--                               data-callback="openLogin"--}}
{{--                            >--}}
{{--                                <span>Я прочитала правила</span>--}}
{{--                            </a>--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6">--}}
{{--                    <img class="img-fluid d-flex mx-auto" src="img/about.png" alt="">--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}
{{--    @include('app.layouts.components.about')--}}
{{--    @include('app.layouts.components.footer')--}}
@endsection
