@extends('app.layouts.app')

@section('content')
    <div class="oz-body-wrap {{ $body ?? '' }}">
        @include('app.layouts.components.header')
        <section class="about-generic-area">
            <div class="container border-top-generic">
                {{-- VUE --}}
                <div id="app"></div>
                {{-- VUE --}}
            </div>
        </section>


    </div>
@endsection
