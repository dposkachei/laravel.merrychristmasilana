<!-- Start Banner Area -->
<section class="banner-area relative"
{{--         style="background-image: url({{ asset('img/ezgif-4-4431e73ed0d2.gif') }}); background-repeat: no-repeat;background-size: 100%;"--}}
>
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-center " >
            <div class="col-12">
                <div class="story-content"
                    {{--                     style="padding: 10px; background-color: #ccc"--}}
                >
                    <h6 class="text-uppercase">
                        Добро пожаловать на квест
                    </h6>
                    <h1>
                        "
                        С <span class="sp-1">Новым</span> Годом
                        <br>
                        и Счастливого Рождества <span class="sp-2">Дурында</span>
                        "
                    </h1>
                </div>
            </div>
            <div class="banner-left col-lg-6">

{{--                <img class="d-flex mx-auto img-fluid" src="img/header-img.png" alt="">--}}
                <img class="d-flex mx-auto img-fluid" src="{{ asset('img/ezgif-4-4431e73ed0d2.gif') }}" alt="">
            </div>
            <div class="col-lg-6">
                <div class="row justify-content-center text-center">
                    <div class="single-contact col-lg-6 col-md-8">
                        <h2 class="text-white">
                            Войти
                        </h2>
                        <p class="text-white">
                            Логин и паролб
                        </p>
                    </div>
                </div>
                <form action="{{ url('/auth/login') }}" method="post" class="contact-form ajax-form">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <input name="email" placeholder="Email" class="common-input mt-20" required="" type="email">
                        </div>
                        <div class="col-12">
                            <input name="password" placeholder="Пароль" class="common-input mt-20" required="" type="password">
                        </div>
                        <div class="col-12 d-flex justify-content-end">
                            <button class="primary-btn d-inline-flex align-items-center mt-20 inner-form-submit" type="submit">
                                <span>Войти</span>
                                {{--                        <span class="lnr lnr-arrow-right"></span>--}}
                            </button>
                            <br>
                        </div>
                    </div>
                </form>





            </div>
        </div>
    </div>
</section>
<!-- End Banner Area -->
