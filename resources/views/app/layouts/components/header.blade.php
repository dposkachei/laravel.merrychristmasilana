<!-- Start Header Area -->
<header class="default-header">
    <div class="container-fluid">
        <div class="header-wrap">
            <div class="header-top d-flex justify-content-between align-items-center">
                <div class="logo">
                    <a href="{{ config('app.url') }}">
                        <img src="/img/ildur.jpg" alt="" style="width: 50px;height: 50px;border-radius: 100%;">
                    </a>
                </div>
                <div class="main-menubar d-flex align-items-center">
                    <nav class="hide">
                        <a href="{{ route('logout.post') }}">Выйти</a>
                    </nav>
                    <div class="menu-bar">
                        <span class="lnr lnr-menu"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- End Header Area -->
