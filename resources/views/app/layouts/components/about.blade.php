<!-- Start about Area -->
<section class="about-area pt-50">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="story-content">
                    <h2>
                        Lorem ipsum
                        <br>
                        dolor <span>Sit</span>
                    </h2>
                    <p class="mt-30">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium ad aliquam, amet architecto culpa cum deleniti deserunt eos excepturi expedita explicabo illo illum inventore iste modi placeat repellat repudiandae sed similique vero, voluptatem voluptatum? Alias aliquam asperiores culpa cupiditate dolorum eius enim error expedita facilis, id inventore iste itaque minus molestias necessitatibus nobis nulla pariatur veniam! Impedit possimus, sunt?
                        <br> <br>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet aperiam aspernatur culpa dolorem molestias natus non, quam quo repellat sapiente!
                    </p>
                    <a href="#" class="genric-btn primary-border circle arrow">View More<span class="lnr lnr-arrow-right"></span></a>
                </div>
            </div>
            <div class="col-lg-6">
                <img class="img-fluid d-flex mx-auto" src="img/about.png" alt="">
            </div>
        </div>
    </div>
</section>
<!-- End Team Force Area -->
