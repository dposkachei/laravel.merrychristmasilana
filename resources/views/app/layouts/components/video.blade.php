<!-- Start Video Area -->
<section class="video-area pt-40 pb-40"
         style="background: url({{ asset('img/ezgif-4-4431e73ed0d2.gif') }}) no-repeat center center/cover;"
>
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="video-content">
            <a href="http://www.youtube.com/watch?v=0O2aH4XLbto" class="play-btn">
                <img src="{{ asset('img/play-btn.png') }}" alt="">
            </a>
            <div class="video-desc">
                <h3 class="h2 text-white text-uppercase">
                    Being unique is the preference
                </h3>
                <h4 class="text-white">
                    Youtube video will appear in popover
                </h4>
            </div>
        </div>
    </div>
</section>
<!-- End Video Area -->
