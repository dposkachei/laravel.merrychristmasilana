<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="echo" content="1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="shortcut icon" href="{{{ asset('img/favicon.png') }}}">

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,500" rel="stylesheet">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @if(file_exists(public_path('css/vue.css')))
        <link href="{{ asset('css/vue.css') }}" rel="stylesheet">
    @endif
    <script>
        window.Laravel = {
            app: {
                csrf: '{{ csrf_token() }}',
            },
            echo: {
                listen: false,
                enabled: false,
            },
            item: {
                id: '', {{-- заносится в шаблоне сущности --}}
            },
        };
    </script>
</head>
<body>
    @yield('content')
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
