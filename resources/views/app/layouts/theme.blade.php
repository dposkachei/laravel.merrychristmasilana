<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('img/logo.png') }}">
    <meta charset="UTF-8">
{{--    {!! MetaTag::render() !!}--}}

    <title>{{ config('app.name') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,500" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
<div class="oz-body-wrap {{ $body ?? '' }}">
    @include('app.layouts.components.header')
    @yield('content')
    @include('app.layouts.components.footer')
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
