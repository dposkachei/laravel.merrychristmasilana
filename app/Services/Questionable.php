<?php

namespace App\Services;

use App\Events\QuestionFinishedEvent;
use App\Events\QuestionStartedEvent;
use App\Models\Question;
use App\Models\User;

trait Questionable
{
    /**
     * @param Question $question
     * @param string $answer
     * @return bool
     */
    protected function checkAnswer(Question $question, string $answer): bool
    {
        return trim(mb_strtolower($question->answer)) === trim(mb_strtolower($answer));
    }

    /**
     * @param $oUser
     * @return mixed
     */
    protected function getNextQuestion($oUser)
    {
        $oQuestions = $this->getQuestions();
        $oLastQuestion = $oUser->lastQuestion();
        if (is_null($oLastQuestion)) {
            return null;
        }
        $oQuestion = $oQuestions->where('number', $oLastQuestion->number + 1)->first();
        return $oQuestion;
    }

    /**
     * @return mixed
     */
    protected function getQuestions()
    {
        return Question::active()->ordered()->get();
    }

    /**
     * @param Question $oQuestion
     * @param User $oUser
     * @return null|Question
     */
    protected function successAnswer(Question $oQuestion, User $oUser): ?Question
    {
        // завершить вопрос
        event(new QuestionFinishedEvent($oQuestion->id, $oUser->id));

        // следующий вопрос
        $oNewQuestion = $oUser->nextQuestion();
        if (!is_null($oNewQuestion)) {
            event(new QuestionStartedEvent($oNewQuestion->id, $oUser->id));
        }
        return $oNewQuestion;
    }
}
