<?php

namespace App\Services;


trait Messageable
{
    private $sessionWrong = 'answer-wrong';
    private $sessionSuccess = 'answer-success';

    /**
     * @return string
     */
    public function wrongMessage()
    {
        $try = session()->get($this->sessionWrong) ?? 1;
        $try = rand(1, 6);

        $answer = 'Неправильный ответ';
        if ($try === 1) {
            $this->saveTryWrong($try);
            return $answer;
        }
        if ($try === 2) {
            $this->saveTryWrong($try);
            return 'Да правда неправильный ответ';
        }
        if ($try === 3) {
            $this->saveTryWrong($try);
            return 'Ну ты че? Неправильный ответ';
        }
        if ($try === 4) {
            $this->saveTryWrong($try);
            return 'Але? Опять?';
        }
        if ($try === 5) {
            $this->saveTryWrong($try);
            return 'Неправильно. У меня нет слов.';
        }
        if ($try === 6) {
            $this->saveTryWrong(0);
            return '...';
        }
        return $answer;
    }

    /**
     * @return string
     */
    public function successMessage()
    {
        $try = session()->get($this->sessionSuccess) ?? 1;
        $try = rand(1, 6);

        $answer = 'Ну ничосе, идем дальше';
        if ($try === 1) {
            $this->saveTrySuccess($try);
            return $answer;
        }
        if ($try === 2) {
            $this->saveTrySuccess($try);
            return 'Харош, мозг!';
        }
        if ($try === 3) {
            $this->saveTrySuccess($try);
            return 'Мега мозг, если че';
        }
        if ($try === 4) {
            $this->saveTrySuccess($try);
            return 'Красава';
        }
        if ($try === 5) {
            $this->saveTrySuccess($try);
            return 'Юхуууууу, вот это умница';
        }
        if ($try === 6) {
            $this->saveTrySuccess(0);
            return 'Мухахаха ха а ха х аа, ничтяк';
        }
        return $answer;
    }

    /**
     * @param int $try
     */
    private function saveTryWrong(int $try): void
    {
        $try++;
        session()->put($this->sessionWrong, $try);
    }

    /**
     * @param int $try
     */
    private function saveTrySuccess(int $try): void
    {
        $try++;
        session()->put($this->sessionSuccess, $try);
    }

}
