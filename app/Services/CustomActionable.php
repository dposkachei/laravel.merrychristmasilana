<?php

namespace App\Services;

trait CustomActionable
{
    /**
     * @return array
     */
    public function actionConfirmRules()
    {
        session()->put('confirm-rules', 1);
        return responseCommon()->success([], 'Я буду следить');
    }
}
