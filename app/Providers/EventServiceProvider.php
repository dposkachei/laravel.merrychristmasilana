<?php

namespace App\Providers;

use App\Events\QuestionStartedEvent;
use App\Events\QuestionFinishedEvent;
use App\Listeners\QuestionStartedListener;
use App\Listeners\QuestionFinishedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        QuestionStartedEvent::class => [
            QuestionStartedListener::class,
        ],
        QuestionFinishedEvent::class => [
            QuestionFinishedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
