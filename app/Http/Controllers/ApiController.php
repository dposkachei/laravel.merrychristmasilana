<?php

namespace App\Http\Controllers;

use App\Events\QuestionFinishedEvent;
use App\Events\QuestionStartedEvent;
use App\Http\Controllers\Transformers\QuestionTransformer;
use App\Models\Question;
use App\Models\QuestionHint;
use App\Models\User;
use App\Services\CustomActionable;
use App\Services\Messageable;
use App\Services\Questionable;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    use Helpers;
    use Messageable;
    use Questionable;

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        return $this->response->noContent();
    }

    /**
     * @return array
     */
    public function data(Request $request)
    {
        $oUser = User::first();

        $oQuestions = Question::with('hints')->active()->ordered()->get();

        $oQuestion = $oUser->currentQuestion();

        $aQuestion = (new QuestionTransformer())->transform($oQuestion);
        $aQuestions = $oQuestions->transform(function ($item) {
            return (new QuestionTransformer())->transform($item);
        })->all();

        $oFirstStatisticQuestion = $oUser->firstStatisticQuestion();

        return responseCommon()->success([
            'data' => [
                'questions' => $aQuestions,
                'question' => $aQuestion,
                'started_at' => $oFirstStatisticQuestion->started_at->addHour()->format('Y-m-d H:i:s'),
                //'started_at' => 'December 25, 2019',
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function answer(Request $request)
    {
        $oUser = User::first();

        $question_id = $request->get('question_id');
        $answer = $request->get('answer');
        $oQuestion = Question::find($question_id);

        if (!$this->checkAnswer($oQuestion, $answer)) {
            return responseCommon()->jsonError([
                'message' => $this->wrongMessage(),
            ]);
        }
        $oNewQuestion = $this->successAnswer($oQuestion, $oUser);

        if (is_null($oNewQuestion)) {
            $this->resetQuest($oUser);
            return responseCommon()->success([
                'data' => [
                    'final' => true,
                ],
            ]);
        }
        return responseCommon()->success([
            'data' => [
                'question' => (new QuestionTransformer())->transform($oNewQuestion),
            ],
        ], $this->successMessage());
    }

    public function resetQuest($oUser)
    {
        $oUser->clearStatistics();
        $oQuestionHints = QuestionHint::where('status', 2)->get();
        foreach ($oQuestionHints as $oQuestionHint) {
            $oQuestionHint->update([
                'status' => 1,
            ]);
        }
    }

    /**
     * @param Request $request
     * @return array
     */
    public function hint(Request $request)
    {
        $question_id = $request->get('question_id');
        $oQuestion = Question::find($question_id);
        $oHint = $oQuestion->hints()->first();
        if (!is_null($oHint)) {
            $oHint->update([
                'status' => 2,
            ]);
            return responseCommon()->success([], $oHint->text);
        }
        return responseCommon()->success([], 'А нет больше подсказок(');
    }

}
