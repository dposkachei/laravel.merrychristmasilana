<?php

namespace App\Http\Controllers;

use App\Events\QuestionStartedEvent;
use App\Models\Question;
use App\Models\User;
use App\Services\CustomActionable;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    use CustomActionable;

    public function index()
    {
        return view('app.index');
    }

    public function lk()
    {
        $oUser = User::first();
        $oQuestion = $oUser->currentQuestion();
        if (is_null($oQuestion)) {
            $oQuestion = Question::active()->ordered()->first();
            event(new QuestionStartedEvent($oQuestion->id, $oUser->id));
        }
        return view('app.lk');
    }


    public function checkDate()
    {
        return false;
    }

    public function message()
    {
        $data = [
            'number' => 1,
            'title' => 'Первое задание',
            'description' => 'Описание первого задания',
            'text' => 'Сама загадка',
        ];

        return $data;
    }



}
