<?php

namespace App\Http\Controllers\Transformers;

use App\Models\Question;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    /**
     * @param Question $oItem
     * @return array
     */
    public function transform(Question $oItem)
    {
        $data = [
            'id' => $oItem->id,
            'number' => $oItem->number,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'description' => $oItem->description,
            'purpose' => $oItem->purpose,
            'hints' => [
                'count' => $oItem->allHints()->count(),
                'used' => $oItem->usedHints()->count(),
            ]
        ];
        return $data;
    }
}
