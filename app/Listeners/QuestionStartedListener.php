<?php

namespace App\Listeners;

use App\Events\QuestionStartedEvent;
use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Events\Attempting as Event;

class QuestionStartedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param QuestionStartedEvent $event
     * @return void
     */
    public function handle(QuestionStartedEvent $event)
    {
        $oUser = User::find($event->user_id);
        $oQuestion = Question::find($event->question_id);

        $oUser->statistics()->create([
            'question_id' => $oQuestion->id,
            'started_at' => now(),
        ]);
        info('Загадка: ' . $oQuestion->title . ' - начата');
    }
}
