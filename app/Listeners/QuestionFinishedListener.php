<?php

namespace App\Listeners;

use App\Events\QuestionFinishedEvent;
use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Events\Attempting as Event;

class QuestionFinishedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param QuestionFinishedEvent $event
     * @return void
     */
    public function handle(QuestionFinishedEvent $event)
    {
        $oUser = User::find($event->user_id);
        $oQuestion = Question::find($event->question_id);

        $oUserStatistic = $oUser->statistics()
            ->where('question_id', $oQuestion->id)
            ->whereNull('finished_at')
            ->first();

        if (!is_null($oUserStatistic)) {
            $oUserStatistic->update([
                'finished_at' => now(),
            ]);
            $diff = $oUserStatistic->started_at->diffForHumans($oUserStatistic->finished_at);
            info('Загадка: ' . $oQuestion->title . ' - разгадана за ' . $diff);
        }
    }
}
