<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function statistics()
    {
        return $this->hasMany(UserStatistic::class)->where('status', 1)->orderBy('created_at');
    }

    public function statistic()
    {
        return $this->hasMany(UserStatistic::class)->orderBy('created_at');
    }

    public function currentQuestion()
    {
        $oStatistic = $this->statistics()->whereNull('finished_at')->getResults()->first();
        if (is_null($oStatistic)) {
            return null;
        }
        return $oStatistic->question;
    }

    public function nextQuestion()
    {
        $oStatistic = $this->statistics()->whereNotNull('finished_at')->getResults()->last();
        if (is_null($oStatistic)) {
            return null;
        }
        $oLastQuestion = $oStatistic->question;
        $oQuestion = Question::active()->where('number', $oLastQuestion->number + 1)->ordered()->first();
        if (is_null($oQuestion)) {
            $oQuestion = Question::active()->where('number', $oLastQuestion->number + 2)->ordered()->first();
            if (is_null($oQuestion)) {
                $oQuestion = Question::active()->where('number', $oLastQuestion->number + 3)->ordered()->first();
            }
        }
        return $oQuestion;
    }

    public function lastQuestion()
    {
        $oStatistics = $this->statistics()->getResults();
        if (count($oStatistics) === 0) {
            return null;
        }
        $oStatistics->load('question');
        $oStatistics = $oStatistics->sortByDesc(function ($item) {
            return $item->question->number;
        });
        $oStatistic = $oStatistics->first();
        if (is_null($oStatistic)) {
            return null;
        }
        return $oStatistic->question;
    }

    public function firstStatisticQuestion()
    {
        $oStatistics = $this->statistics()->getResults();
        if (count($oStatistics) === 0) {
            return null;
        }
        $oStatistics->load('question');
        $oStatistics = $oStatistics->sortBy(function ($item) {
            return $item->question->number;
        });
        $oStatistic = $oStatistics->first();
        if (is_null($oStatistic)) {
            return null;
        }
        return $oStatistic;
    }

    public function clearStatistics()
    {
        $this->statistics()->delete();
    }
}
