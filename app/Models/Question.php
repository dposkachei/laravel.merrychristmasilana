<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Question extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'description', 'number',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('number', 'asc');
    }

    /**
     * @return mixed
     */
    public function allHints()
    {
        return $this->hasMany(QuestionHint::class)->whereIn('status', [1, 2])->ordered();
    }

    /**
     * @return mixed
     */
    public function hints()
    {
        return $this->hasMany(QuestionHint::class)->where('status', 1)->ordered();
    }

    /**
     * @return mixed
     */
    public function usedHints()
    {
        return $this->hasMany(QuestionHint::class)->where('status', 2)->ordered();
    }
}
