const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const TYPE = 'app'; // app

const FILES = {
    app: {
        css: {
            from: 'resources/sass/app/bootstrap/app/app.scss',
            to: 'public/css',
        },
        js: {
            from: 'resources/js/app/app.js',
            to: 'public/js',
        }
    }
};

const MIX = {

    mix: null,

    config: {
        css: {
            from: '',
            to: '',
        },
        js: {
            from: '',
            to: '',
        },
    },

    run(mix) {
        let self = this;
        self.mix = mix;
        return self;
    },
    app() {
        let self = this;
        self.config.css = FILES.app.css;
        self.config.js = FILES.app.js;

        self.mix.js(self.config.js.from, self.config.js.to);
        return self;
    },
    development(watchUrl) {
        let self = this;
        self.mix.webpackConfig({
            devtool: "inline-source-map"
        });
        self.mix.sass(self.config.css.from, self.config.css.to).options({
            autoprefixer: {
                options: {
                    browsers: [
                        'last 6 versions',
                    ]
                }
            },
            postCss: [
                require('postcss-font-magician'),
                require('css-mqpacker'),
                require('postcss-remove-root'),
            ],
            processCssUrls: true,
            extractVueStyles: 'css/vue.css',
        }).sourceMaps();
        self.mix.browserSync(watchUrl);
        self.mix.disableNotifications();
    },
    production() {
        let self = this;
        self.mix.sass(self.config.css.from, self.config.css.to).options({
            autoprefixer: {
                options: {
                    browsers: [
                        'last 6 versions',
                    ]
                }
            },
            postCss: [
                require('cssnano'),
                require('postcss-font-magician'),
                require('css-mqpacker'),
                require('postcss-remove-root'),
            ],
            processCssUrls: true,
            clearConsole: true,
        }).version();
    }
};

if (!mix.inProduction()) {
    MIX.run(mix).app().development(process.env.APP_URL);
} else {
    MIX.run(mix).app().production();
}

